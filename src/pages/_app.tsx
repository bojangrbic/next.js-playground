import { NextPage } from 'next/types';
import AuthProvider, { useAuth } from 'src/context/auth';
import { Layout } from 'src/components/layout';

export type RhAuthProps = {
  required: boolean;
  loader?: JSX.Element;
};

export type RhNextPage = NextPage & {
  auth?: RhAuthProps;
};

export type RhAppProps = {
  Component: RhNextPage;
  pageProps: any;
};

interface IRhCheckAuthProps {
  auth: RhAuthProps;
  children: JSX.Element;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function RhCheckAuth({ auth, children }: IRhCheckAuthProps): JSX.Element {
  const { loading, user } = useAuth();

  if (loading) {
    return (
      <h1>Loading auth</h1>
    );
  }

  if (!user) {
    return (
      <h1>You must be logged in</h1>
    );
  }

  return children;
}

export default function MyApp({ Component, pageProps }: RhAppProps) {
  return (
    <AuthProvider>
      <Layout>
        {Component.auth ? (
          <RhCheckAuth auth={Component.auth}><Component {...pageProps} /></RhCheckAuth>
        ) : (
          <Component {...pageProps} />
        )}
      </Layout>
    </AuthProvider>
  );
}
