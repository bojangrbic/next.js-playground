import { NextApiHandler } from 'next';

import { validateAccessToken } from 'src/utils/auth';
import axios from 'axios';

const handler: NextApiHandler = async (req, res) => {
  const { method } = req;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'GET') {
      return res.status(405).send('Method Not Allowed');
    }

    const authHeader = String(req.headers.authorization || '');

    if (!authHeader || !authHeader.startsWith('Bearer ')) {
      return res.status(400).send('No authorization header');
    }

    const token = authHeader.substring(7, authHeader.length);

    if (!token) {
      return res.status(400).send('No authorization token');
    }

    validateAccessToken(token);

    const { data } = await axios.get('https://dummyjson.com/posts');

    return res.status(200).send(data.posts);
  } catch (e) {
    console.error(e);
    return res.status(401).send('Validate user error');
  }
};

export default handler;
