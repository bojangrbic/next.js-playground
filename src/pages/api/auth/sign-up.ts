import bcrypt from 'bcrypt';
import { NextApiHandler } from 'next';

import { firstNameValid, lastNameValid, emailValid, passwordValid } from 'src/utils/validation';
import prisma from 'prisma/index';

const handler: NextApiHandler = async (req, res) => {
  const { method, body } = req;
  const { firstName, lastName, email, password } = body;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'POST') {
      return res.status(405).send('Method Not Allowed');
    }

    if (!firstName || !firstNameValid(firstName)) {
      return res.status(400).send('Mising firstName');
    }

    if (!lastName || !lastNameValid(lastName)) {
      return res.status(400).send('Mising lastName');
    }

    if (!email || !emailValid(email)) {
      return res.status(400).send('Mising email');
    }

    if (!password || !passwordValid(password)) {
      return res.status(400).send('Mising password');
    }

    const userExist = await prisma.user.findFirst({
      where: { email },
      rejectOnNotFound: false
    });

    if (userExist) {
      return res.status(400).send('User already exist');
    }

    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);
    const cryptedPassword = bcrypt.hashSync(password, salt);

    await prisma.user.create({
      data: {
        firstName,
        lastName,
        email,
        password: cryptedPassword,
        enabled: true
      }
    });

    return res.status(200).send('User created');
  } catch (e) {
    console.error(e);
    return res.status(500).send('Sign up error');
  }
};

export default handler;
