import { NextApiHandler } from 'next';

import { validateRefreshToken, createToken } from 'src/utils/auth';
import prisma from 'prisma/index';

const handler: NextApiHandler = async (req, res) => {
  const { method, body } = req;
  const { token } = body;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'POST') {
      return res.status(405).send('Method Not Allowed');
    }

    if (!token) {
      return res.status(400).send('Mising token');
    }

    const decoded = validateRefreshToken(token);
    const { email } = decoded;

    const user = await prisma.user.findFirst({
      where: {
        email
      },
      rejectOnNotFound: false
    });

    if (!user) {
      return res.status(400).send('Mising user');
    }

    const { accessToken, refreshToken } = createToken(user);

    return res.status(200).json({
      accessToken,
      refreshToken
    });
  } catch (e) {
    console.error(e);
    return res.status(500).send('Refresh token error');
  }
};

export default handler;
