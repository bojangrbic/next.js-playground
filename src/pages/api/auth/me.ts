import { NextApiHandler } from 'next';

import { validateAccessToken } from 'src/utils/auth';
import prisma from 'prisma/index';

const handler: NextApiHandler = async (req, res) => {
  const { method } = req;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'GET') {
      return res.status(405).send('Method Not Allowed');
    }

    const authHeader = String(req.headers.authorization || '');

    if (!authHeader || !authHeader.startsWith('Bearer ')) {
      return res.status(400).send('No authorization header');
    }

    const token = authHeader.substring(7, authHeader.length);

    if (!token) {
      return res.status(400).send('No authorization token');
    }

    const decoded = validateAccessToken(token);

    const { email } = decoded;

    const user = await prisma.user.findFirst({
      where: {
        email
      },
      rejectOnNotFound: true
    });

    if (!user) {
      return res.status(400).send('User doesn`t exist');
    }

    return res.status(200).json({
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName
    });
  } catch (e) {
    console.error(e);
    return res.status(401).send('Validate user error');
  }
};

export default handler;
