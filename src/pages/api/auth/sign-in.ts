import bcrypt from 'bcrypt';
import { NextApiHandler } from 'next';

import { createToken } from 'src/utils/auth';
import { emailValid, passwordValid } from 'src/utils/validation';
import prisma from 'prisma/index';

const handler: NextApiHandler = async (req, res) => {
  const { method, body } = req;
  const { email, password } = body;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'POST') {
      return res.status(405).send('Method Not Allowed');
    }

    if (!email || !emailValid(email)) {
      return res.status(400).send('Mising email');
    }

    if (!password || !passwordValid(password)) {
      return res.status(400).send('Mising password');
    }

    const user = await prisma.user.findFirst({
      where: {
        email
      },
      rejectOnNotFound: false
    });

    if (!user) {
      return res.status(400).send('User doesn`t exist');
    }

    const passwordMatch = await bcrypt.compare(password, user.password);

    if (!passwordMatch) {
      return res.status(400).send('Wrong password');
    }

    const { accessToken, refreshToken } = createToken(user);

    return res.status(200).json({
      accessToken,
      refreshToken
    });
  } catch (e) {
    console.error(e);
    return res.status(500).send('Sign in error');
  }
};

export default handler;
