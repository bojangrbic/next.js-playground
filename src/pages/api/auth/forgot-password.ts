/* eslint-disable no-bitwise */
import { NextApiHandler } from 'next';
import { v4 as uuid } from 'uuid';

import { emailValid } from 'src/utils/validation';
import prisma from 'prisma/index';

const handler: NextApiHandler = async (req, res) => {
  const { method, body } = req;
  const { email } = body;
  res.setHeader('Cache-Control', 'no-store');

  try {
    if (method !== 'POST') {
      return res.status(405).send('Method Not Allowed');
    }

    if (!email || !emailValid(email)) {
      return res.status(400).send('Email required');
    }

    const user = await prisma.user.findFirst({
      where: {
        email
      },
      rejectOnNotFound: true
    });

    if (!user) {
      return res.status(400).send('User doesn`t exist');
    }

    const hash = uuid();

    await prisma.user.update({
      where: {
        email
      },
      data: {
        enabled: false,
        password: hash,
        rememberToken: hash
      }
    });

    // TODO Send email
    return res.status(200).json({ message: `Mail sent to ${email}` });
  } catch (e) {
    console.error(e);
    return res.status(500).send('Forgot password error');
  }
};

export default handler;
