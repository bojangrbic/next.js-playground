import axios from 'axios';
import React, { useState } from 'react';

export default function Register() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const [success, setSuccess] = useState('');

  const styleInput = { width: 300, marginTop: 10 };

  const handleRegister = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      await axios.post('/api/auth/sign-up', {
        firstName,
        lastName,
        email,
        password
      });

      setFirstName('');
      setLastName('');
      setEmail('');
      setPassword('');
      setSuccess('User created');
    } catch (error) {
      console.error('handleRegister error', error);

      let message;

      if (axios.isAxiosError(error) && error.response) {
        message = error.response.data as string;
      } else {
        message = String(error);
      }

      setError(message);
    }
  };

  return (
    <form
      onSubmit={handleRegister}
      style={{ display: 'flex', flexDirection: 'column', margin: 20 }}
    >
      <h3>Register</h3>
      <input
        type="text"
        id="firstName"
        name="firstName"
        placeholder="firstName"
        value={firstName}
        style={styleInput}
        onChange={(e) => setFirstName(e.target.value)}
      />
      <input
        type="text"
        id="lastName"
        name="lastName"
        placeholder="lastName"
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
        style={styleInput}
      />
      <input
        type="email"
        id="email"
        name="email"
        placeholder="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        style={styleInput}
      />
      <input
        type="password"
        id="password"
        name="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        style={styleInput}
      />
      {success && <p style={{ color: 'green' }}>{success}</p>}
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <button
        type="submit"
        disabled={!(firstName && lastName && email && password)}
        style={{
            width: 100,
            height: 20,
            marginTop: 15,
            background: 'lightblue'
          }}
      >
        Register
      </button>
    </form>
  );
}
