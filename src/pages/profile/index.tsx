import { useAuth } from 'src/context/auth';

export default function ProfilePage() {
  const { user } = useAuth();

  return (
    <>
      <h1 data-testid="profileTitle">Profile</h1>
      <p>
        Email: {user?.email}
      </p>
      <p>
        First Name: {user?.firstName}
      </p>
      <p>
        Last Name: {user?.lastName}
      </p>
    </>
  );
}

ProfilePage.auth = {
  required: true,
  loading: <div>ProfilePage Loading</div>
};
