import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useAuth } from 'src/context/auth';

export default function Login() {
  const router = useRouter();
  const { logIn } = useAuth();

  const [email, setEmail] = useState('bojan.grbic@htecgroup.com');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const styleInput = { width: 300, marginTop: 10 };

  const handleLogIn = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const { data } = await axios.post('/api/auth/sign-in', {
        email,
        password
      });

      const { accessToken, refreshToken } = data;

      const config = {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      };
      const { data: me } = await axios.get('/api/auth/me', config);

      logIn({ me, accessToken, refreshToken });
      router.push('/profile');
    } catch (error) {
      console.error('handleLogIn error', error);

      let message;

      if (axios.isAxiosError(error) && error.response) {
        message = error.response.data as string;
      } else {
        message = String(error);
      }

      setError(message);
    }
  };

  return (
    <form
      onSubmit={handleLogIn}
      style={{ display: 'flex', flexDirection: 'column', margin: 20 }}
    >
      <h3>Login</h3>
      <input
        type="email"
        id="email"
        name="email"
        placeholder="email"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        style={styleInput}
      />
      <input
        type="password"
        id="password"
        name="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        style={styleInput}
      />
      {error && <p style={{ color: 'red' }}>{error}</p>}
      <button
        type="submit"
        disabled={!(email && password)}
        style={{
            width: 100,
            height: 20,
            marginTop: 15,
            background: 'lightblue'
          }}
      >
        Login
      </button>
    </form>
  );
}
