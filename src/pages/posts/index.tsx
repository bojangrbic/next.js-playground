import { useEffect, useState } from 'react';

import PostsService, { Post } from 'src/services/posts';

export default function PostsPage() {
  const [posts, setPosts] = useState<Post[]>([]);

  useEffect(() => {
    const getPosts = async () => {
      try {
        const data = await PostsService.get();
        setPosts(data);
      } catch (e) {
        //
      }
    };

    getPosts();
  }, []);

  return (
    <>
      <h1 data-testid="profileTitle">Posts</h1>
      <ul>
        {posts.map((post) => <li key={post.id}>{post.title}</li>)}
      </ul>
    </>
  );
}

PostsPage.auth = {
  required: true,
  loading: <div>ProfilePage Loading</div>
};
