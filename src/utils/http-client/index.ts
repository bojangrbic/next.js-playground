/* eslint-disable no-param-reassign */
/* eslint-disable class-methods-use-this */
import axios from 'axios';
import cookie from 'js-cookie';

import type { AxiosError, AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';

export interface AxiosRequestConfigExtended extends AxiosRequestConfig {
  headers?: {
    Authorization?: string;
  };
}

export function isAxiosError(value: any): value is AxiosError {
  return typeof value?.response === 'object';
}

export interface HttpClientInstanceConfig {
  withInterceptors?: boolean;
}

export abstract class HttpClient {
  private instance: AxiosInstance;

  protected constructor(baseURL: string) {
    this.instance = axios.create({ baseURL });
  }

  protected async onRequest(config: AxiosRequestConfig): Promise<AxiosRequestConfig> {
    const accessToken = cookie.get('accessToken');

    if (accessToken) {
      if (!config.headers) config.headers = {};
      config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return config;
  }

  protected onRequestError(error: AxiosError): Promise<AxiosError> {
    return Promise.reject(error);
  }

  protected onResponse(response: AxiosResponse): AxiosResponse {
    return response;
  }

  protected onResponseError(error: AxiosError) {
    const refreshToken = cookie.get('refreshToken');

    const originalRequest = error.config;

    if (refreshToken && error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;

      return this.instance
        .post('/api/auth/refresh-token', {
          token: refreshToken
        })
        .then(async (res) => {
          if (res.status === 200) {
            cookie.set('accessToken', res.data.accessToken, { expires: 1 });
            cookie.set('refreshToken', res.data.refreshToken, { expires: 1 });

            console.info('Access token refreshed!');
            return this.instance(originalRequest);
          }
        });
    }

    return Promise.reject(error);
  }

  protected setupInterceptors() {
    this.instance.interceptors.request.use(
      async (request) => this.onRequest(request),
      (error) => this.onRequestError(error)
    );

    this.instance.interceptors.response.use(
      (response) => this.onResponse(response),
      (error) => this.onResponseError(error)
    );
  }

  protected getInstance(config: HttpClientInstanceConfig = { withInterceptors: true }) {
    const { withInterceptors } = config;

    if (withInterceptors) {
      this.setupInterceptors();
    }

    return this.instance;
  }
}
