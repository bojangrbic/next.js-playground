/* eslint-disable max-len */
import { User } from '@prisma/client';
import * as jwt from 'jsonwebtoken';
import { ACCESS_TOKEN_LIFETIME, ACCESS_TOKEN_SECRET, REFRESH_TOKEN_LIFETIME, REFRESH_TOKEN_SECRET } from 'src/config';

import { IJwtPayload } from 'types';

export type AuthUser = Pick<User, 'id' | 'email' | 'firstName' | 'lastName'>;

export const createToken = (user: User): {accessToken: string, refreshToken: string } => {
  const accessToken = jwt.sign(
    {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName
    },
    ACCESS_TOKEN_SECRET,
    { expiresIn: ACCESS_TOKEN_LIFETIME }
  );

  const refreshToken = jwt.sign(
    {
      id: user.id,
      email: user.email,
      firstname: user.firstName,
      lastname: user.lastName
    },
    REFRESH_TOKEN_SECRET,
    { expiresIn: REFRESH_TOKEN_LIFETIME }
  );

  return { accessToken, refreshToken };
};

export const validateAccessToken = (token: string): IJwtPayload => jwt.verify(token, ACCESS_TOKEN_SECRET) as IJwtPayload;

export const validateRefreshToken = (token: string): IJwtPayload => jwt.verify(token, REFRESH_TOKEN_SECRET) as IJwtPayload;
