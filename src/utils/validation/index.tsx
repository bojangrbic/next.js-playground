export const firstNameValid = (val: string): boolean => val.length > 3;

export const lastNameValid = (val: string): boolean => val.length > 3;

export const emailValid = (val: string): boolean => val.indexOf('@') !== -1;

export const passwordValid = (val: string): boolean => val.length > 5;

export const passwordRepeatValid = (val: string): boolean => val.length > 5;
