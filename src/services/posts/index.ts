import { HttpClient } from 'src/utils/http-client';

export type Post = {
  id: number;
  title: string;
  body: string;
  userId: 9;
  tags: string[];
  reactions: number;
}

class Auth extends HttpClient {
  private static BASE_URL = `${process.env.NEXT_PUBLIC_APP_BASE_URL || ''}`;

  public constructor() {
    super(Auth.BASE_URL);
  }

  public get = async (): Promise<Post[]> => {
    const client = this.getInstance();

    const { data } = await client.get('/api/posts');

    return data;
  };
}

export default new Auth();
