import { User } from '@prisma/client';
import { HttpClient } from 'src/utils/http-client';

export type AuthUser = Pick<User, 'id' | 'email' | 'firstName' | 'lastName'>;

export type SignInResponse = {
  user: AuthUser;
  accessToken: string;
  refreshToken: string;
}

class Auth extends HttpClient {
  private static BASE_URL = `${process.env.NEXT_PUBLIC_APP_BASE_URL || ''}`;

  public constructor() {
    super(Auth.BASE_URL);
  }

  public signUp = async (
    firstName: string,
    lastName: string,
    email: string,
    password: string
  ): Promise<string> => {
    const client = this.getInstance({ withInterceptors: false });

    const { data } = await client.post('/api/auth/sign-up', {
      firstName,
      lastName,
      email,
      password
    });

    return data;
  };

  public signIn = async (
    email: string,
    password: string
  ): Promise<SignInResponse> => {
    const client = this.getInstance({ withInterceptors: false });

    const { data } = await client.post('/api/auth/sign-in', {
      email,
      password
    });

    return data;
  };

  public me = async (): Promise<AuthUser> => {
    const client = this.getInstance();

    const { data } = await client.get('/api/auth/me');

    return data;
  };
}

export default new Auth();
