import Head from 'next/head';
import Link from 'next/link';
import { FC, ReactNode } from 'react';
import { useAuth } from 'src/context/auth';

type LayoutPros = {
  children: ReactNode;
}

export const Layout: FC<LayoutPros> = ({ children }) => {
  const { user, logOut } = useAuth();

  return (
    <>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <header
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            margin: '0 0 20px 0',
            padding: '20px',
            borderBottom: '1px solid gray'
          }}
        >
          <ul
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              flexDirection: 'row',
              margin: 0,
              padding: 0,
              listStyle: 'none'
            }}
          >
            <li>
              <Link href="/" passHref>
                <a style={{ padding: '10px' }}>Home</a>
              </Link>
            </li>
          </ul>
          <ul
            style={{
              display: 'flex',
              justifyContent: 'flex-end',
              flexDirection: 'row',
              margin: 0,
              padding: 0,
              listStyle: 'none'
            }}
          >
            {user ? (
              <>
                <li>
                  <Link href="/posts" passHref>
                    <a style={{ padding: '10px' }}>Posts</a>
                  </Link>
                </li>
                <li>
                  <Link href="/profile" passHref>
                    <a style={{ padding: '10px' }}>Profile</a>
                  </Link>
                </li>
                <li>
                  <button type="button" onClick={logOut}>Log out</button>
                </li>
              </>
            ) : (
              <>
                <li>
                  <Link href="/login" passHref>
                    <a style={{ padding: '10px' }}>Log in</a>
                  </Link>
                </li>
                <li>
                  <Link href="/register" passHref>
                    <a style={{ padding: '10px' }}>Register</a>
                  </Link>
                </li>
              </>
            )}
          </ul>
        </header>
        {children}
      </main>
    </>
  );
};
