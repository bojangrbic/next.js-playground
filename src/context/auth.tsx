import React, { useState, useContext, createContext, useEffect } from 'react';
import { User } from '@prisma/client';
import cookie from 'js-cookie';

import AuthService from 'src/services/auth/index';

export type AuthUser = Pick<User, 'id' | 'email' | 'firstName' | 'lastName'>;

type AuthContextType = {
  loading: boolean
  user: AuthUser | undefined,
  logIn: ({ me, accessToken, refreshToken }: { me: AuthUser; accessToken: string; refreshToken: string; }) => void
  logOut: () => void
}

const initialState = {
  loading: false,
  user: undefined,
  logIn: ({ me, accessToken, refreshToken }: { me: AuthUser; accessToken: string; refreshToken: string; }) => {},
  logOut: () => {}
};

export const AuthContext = createContext<AuthContextType>(initialState);

type Props = {
  children?: React.ReactNode;
}

export function AuthProvider({ children }: Props) {
  const [loading, setLoading] = useState(false);
  const [user, setUser] = useState<AuthUser | undefined>();

  useEffect(() => {
    const getMyData = async () => {
      try {
        setLoading(true);
        const me = await AuthService.me();
        setUser(me);
      } catch (e) {
        //
      } finally {
        setLoading(false);
      }
    };

    getMyData();
  }, []);

  const logIn = ({ me, accessToken, refreshToken }: { me: AuthUser; accessToken: string; refreshToken: string; }) => {
    cookie.set('accessToken', accessToken, { expires: 1 });
    cookie.set('refreshToken', refreshToken, { expires: 1 });
    setUser(me);
  };

  const logOut = () => {
    cookie.remove('accessToken');
    cookie.remove('refreshToken');
    setUser(undefined);
  };

  return (
    <AuthContext.Provider
      value={{
        loading,
        user,
        logIn,
        logOut
      }}
    >
      {children}
    </AuthContext.Provider>
  );
}

export const useAuth = (): AuthContextType => {
  const { loading, user, logIn, logOut } = useContext(AuthContext);
  return { loading, user, logIn, logOut };
};

export default AuthProvider;
